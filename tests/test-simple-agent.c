/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <errno.h>
#include <string.h>
#include <unistd.h>

#include "helloworld-simple-agent/helloworld-tick-board.h"

typedef struct
{
  GMainLoop *loop;              /* owned */
  GDBusConnection *conn;        /* owned */
  HlwDBusTickBoard *tick_board; /* owned */
  GSubprocess *subprocess;      /* owned */
  guint name_watch_id;
} Test;

static void
proxy_new_cb (GObject *source_object,
              GAsyncResult *res,
              gpointer user_data)
{
  GError *error = NULL;
  Test *test = user_data;

  test->tick_board = hlw_dbus_tick_board_proxy_new_finish (res, &error);

  g_test_message ("Created TickBoard Proxy (%p).", test->tick_board);
}

static void
bus_name_appeared (GDBusConnection *connection,
                   const gchar *name,
                   const gchar *name_owner,
                   gpointer user_data)
{
  Test *test = user_data;

  g_assert_cmpstr (name, ==, BUNDLE_ID);

  g_test_message ("Bus name %s now owned by %s", name, name_owner);

  /* The TickBoard will be created asynchronously. */
  hlw_dbus_tick_board_proxy_new (connection,
                                 G_DBUS_PROXY_FLAGS_NONE,
                                 BUNDLE_ID,
                                 "/org/apertis/HelloWorld/SimpleAgent/TickBoard",
                                 NULL,
                                 proxy_new_cb, test);
}

static void
bus_name_vanished (GDBusConnection *connection,
                   const gchar *name,
                   gpointer user_data)
{
  g_assert_cmpstr (name, ==, BUNDLE_ID);

  g_test_message ("Bus name (%s) vanished.", name);
}

static void
setup (Test *test,
       gconstpointer unused)
{
  g_autoptr (GError) error = NULL;
  const gchar *simple_agent;
  GBusNameWatcherFlags watch_flags;

  test->loop = g_main_loop_new (NULL, FALSE);

  if (g_getenv ("TESTS_UNINSTALLED") == NULL)
    {
      /* We are testing an installed copy. Rely on the app framework
       * to make it a properly activatable D-Bus service */
      watch_flags = G_BUS_NAME_WATCHER_FLAGS_AUTO_START;
    }
  else
    {
      /* We are running "make check" without the app framework, so find
       * the executable nearby and run it ourselves */
      simple_agent =
          g_test_get_filename (G_TEST_BUILT,
                               "helloworld-simple-agent/helloworld-simple-agent",
                               NULL);

      g_test_message ("Running an agent in subprocess (path: %s)",
                      simple_agent);

      test->subprocess =
          g_subprocess_new (G_SUBPROCESS_FLAGS_STDOUT_SILENCE, &error,
                            simple_agent,
                            NULL);

      g_assert_no_error (error);

      watch_flags = G_BUS_NAME_WATCHER_FLAGS_NONE;
    }

  test->conn = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &error);
  g_assert_no_error (error);

  test->name_watch_id =
      g_bus_watch_name_on_connection (test->conn,
                                      BUNDLE_ID,
                                      watch_flags,
                                      bus_name_appeared,
                                      bus_name_vanished,
                                      test,
                                      NULL);

  while (test->tick_board == NULL)
    g_main_context_iteration (NULL, TRUE);
}

static void
teardown (Test *test,
          gconstpointer unused)
{
  if (test->name_watch_id != 0)
    {
      g_bus_unwatch_name (test->name_watch_id);
      test->name_watch_id = 0;
    }
  g_clear_pointer (&test->loop, g_main_loop_unref);
  g_clear_object (&test->conn);
  g_clear_object (&test->subprocess);
}

static void
test_get_current_tick (Test *test,
                       gconstpointer unused)
{
  int tick;

  g_object_get (test->tick_board, "current-tick", &tick, NULL);

  g_assert_cmpuint (tick, ==, 0);
}

int
main (int argc, char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/Applications/org.apertis.HelloWorld.SimpleAgent/GetCurrentTick", Test, NULL,
              setup, test_get_current_tick, teardown);
  return g_test_run ();
}
