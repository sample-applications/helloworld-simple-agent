HelloWorld Simple Agent Application
=============================

This is a sample repository to demonstrate how to build an agent-only application
bundle and test it.

Dependencies
============

 - apertis-dev-tools
 - automake
 - build-essential
 - git
 - gnome-common
 - gobject-introspection
 - hotdoc
 - libglib2.0-dev
 - libtool
 - pkg-config
 - policykit-1

Building
========

```
$ ade configure --native
$ ade build --native
$ ade export
```

Note that if you are building for a different device or Apertis version (and in particular
if you are cross-compiling), you should replace `--native` with an appropriate `--sysroot`
or `--device` option. See `ade` documentation for more information.

Building flatpak package from Apertis reference runtime
=======================================================

$ flatpak --user remote-add apertis https://images.apertis.org/flatpak/repo/apertis.flatpakrepo
$ flatpak-builder --user --install --install-deps-from apertis --force-clean --disable-rofiles-fuse build-dir flatpak-recipe.yaml
$ agl-compositor

Execution and Debugging on Target Devices
=========================================

For debugging the application on target device refer below link
https://www.apertis.org/guides/flatpak/#execution-and-debugging-on-target-devices

Testing from the build tree
===========================

If you are running a test program from the build tree, the test can be run by `make check`

```
$ make check

PASS: simple-agent.t 1 /Applications/org.apertis.HelloWorld.SimpleAgent/GetCurrentTick
============================================================================
# TOTAL: 1
# PASS:  1
# SKIP:  0
# XFAIL: 0
# FAIL:  0
# XPASS: 0
# ERROR: 0
============================================================================
```

Making bundle with test programs
================================

To copy test programs into bundle, you should run `ade configure` with  `--enable-installed-tests` option.

```
$ ade configure --native --enable-installed-tests
```

Then, the test programs will be installed in a directory, `$(prefix)/libexec/installed-tests/$(BUNDLE_ID)/`.
You can run `simple-agent.t` for testing by the command below.

```
$ /Applications/org.apertis.HelloWorld.SimpleAgent/libexec/installed-tests/org.apertis.HelloWorld.SimpleAgent/simple-agent.t --tap
# random seed: R02S4801eff65e78c6e2984f5548abae0b50
1..1
# Start of Applications tests
# Start of org.apertis.HelloWorld.SimpleAgent tests
# Running an agent in subprocess (path: /Applications/org.apertis.HelloWorld.SimpleAgent/bin/helloworld-simple-agent)
# Bus name (org.apertis.HelloWorld.SimpleAgent) vanished.
helloworld-simple-agent-Message: Registered D-Bus (path: /org/apertis/HelloWorld/SimpleAgent)
# Bus name org.apertis.HelloWorld.SimpleAgent now owned by :1.15
# Created TickBoard Proxy (0x1f68090).
ok 1 /Applications/org.apertis.HelloWorld.SimpleAgent/GetCurrentTick
# End of org.apertis.HelloWorld.SimpleAgent tests
# End of Applications tests
```

Licensing
=========

This sample application is licensed under the MPLv2; see COPYING for more details.

Bugs
====

Bug reports and (git formatted) patches should be submitted to the Phabricator
instance for Apertis:

http://phabricator.apertis.org/

Contact
=======

See AUTHORS.
